# Digits in noise test
This is a simple interface to measure speech reception thresholds (SRT) to 
digits presented in the presence of speech-shaped noise (the shape itself is obtained from 
the audio tokens).
Data are saved in home directory under diguts_in_noise folder.

# Requirements
This interface has been created using python 3.

you need to clone submodules too, there are two ways to do this:
## Option 1 
- git submodule init
- git submodule update

## Option 2 
- git clone --recurse-submodules git@gitlab.com:jundurraga/pysignal_generator.git

or

- git clone --recurse-submodules https://gitlab.com/jundurraga/pysignal_generator.git


# Install
To run source code you can just open the project (as a folder) in PyCharm. 
Make sure to install all requirements. Importanly, you need to install Qt5, 
needed by PyQt5. This can also be done with PyCharm or pip, the same way you
would install any other package. 

# Run experiment
- Before running an experiment make sure the settings are as desired. 
This is configurated in experiment.py. 
Once you have done so, run experiment.py to begin.