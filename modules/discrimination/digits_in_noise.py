from .set_window import DigitsInNoiseWindow
from PyQt5.QtCore import pyqtSignal
from PyQt5 import QtWidgets
import time


class DigitsInNoiseModule(DigitsInNoiseWindow):
    signal_response = pyqtSignal(str)
    signal_start = pyqtSignal()

    def __init__(self):
        super(DigitsInNoiseModule, self).__init__()

        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.returnPressed.connect(self.on_response)
        ql_response.setEnabled(False)

        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_next")[0]
        pb_start.clicked.connect(self.on_start)
        self._finish_message = 'Finished, thanks'

    def on_response(self):
        _edit_line = self.sender()
        response = _edit_line.text()
        self.signal_response.emit(response)
        _edit_line.setText('')

    def on_start(self):
        _button = self.sender()
        _button.setEnabled(False)
        self.signal_start.emit()
        _button.setText('')

    def on_feedback(self, value: bool):
        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_next")[0]
        if value:
            pb_start.setStyleSheet("font-size:32px; background-color: green")
        else:
            pb_start.setStyleSheet("font-size:32px; background-color: red")
        QtWidgets.QApplication.processEvents()
        time.sleep(0.5)
        pb_start.setStyleSheet("font-size:32px")
        QtWidgets.QApplication.processEvents()

    def on_finish(self):
        pb_start = self.dialog.findChildren(QtWidgets.QPushButton, "q_button_next")[0]
        pb_start.setText(self._finish_message)
        QtWidgets.QApplication.processEvents()
        time.sleep(3)
        self.dialog.close()

    def start_playing(self):
        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.setEnabled(False)

    def stop_playing(self):
        ql_response = self.dialog.findChildren(QtWidgets.QLineEdit, "q_line_edit_answer")[0]
        ql_response.setEnabled(True)
        ql_response.setFocus()

    def set_finish_message(self, value):
        self._finish_message = value

    def get_finish_message(self):
        return self._finish_message

    finish_message = property(get_finish_message, set_finish_message)










