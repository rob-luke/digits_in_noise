from PyQt5 import QtWidgets, QtCore
from PyQt5.QtWidgets import QWidget


class DigitsInNoiseWindow(QWidget):
    def __init__(self):
        super(DigitsInNoiseWindow, self).__init__()
        self.dialog = QtWidgets.QDialog()
        self.dialog.setModal(True)
        self.dialog.setWindowTitle("Digits in Noise")

        main_frame = QtWidgets.QFrame()
        main_layout = QtWidgets.QVBoxLayout(main_frame)
        _frame = self.__set_digits_in_noise_frame()
        main_layout.addWidget(_frame)
        self.dialog.setLayout(main_layout)
        self.dialog.showFullScreen()

    def __set_digits_in_noise_tab(self):
        main_frame = QtWidgets.QFrame()
        main_lay_out = QtWidgets.QVBoxLayout(main_frame)
        tab = QWidget()
        frame = self.__set_digits_in_noise_options()
        main_lay_out.addWidget(frame)
        tab.setLayout(main_lay_out)
        tab.setObjectName('tab_digits in noise')
        return tab

    @staticmethod
    def __set_digits_in_noise_frame():
        frame = QtWidgets.QFrame()
        options_box = QtWidgets.QGridLayout(frame)
        label_message = QtWidgets.QLabel('Type the numbers that you hear')
        label_message.setStyleSheet("font-size:32px")
        label_message.setObjectName('label_message')
        label_message.setAlignment(QtCore.Qt.AlignCenter)
        options_box.addWidget(label_message, *[0, 0])

        qline_edit_answer = QtWidgets.QLineEdit('')
        qline_edit_answer.setObjectName('q_line_edit_answer')
        qline_edit_answer.setStyleSheet("font-size:32px")
        qline_edit_answer.setAlignment(QtCore.Qt.AlignCenter)
        options_box.addWidget(qline_edit_answer, *[1, 0])

        qbutton_next = QtWidgets.QPushButton('Start')
        qbutton_next.setObjectName('q_button_next')
        qbutton_next.setStyleSheet("font-size:32px")
        options_box.addWidget(qbutton_next, *[3, 0])

        return frame
