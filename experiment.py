from PyQt5 import QtWidgets
from PyQt5.QtCore import QObject
import sys
import random
from control.definitions import DigitsInNoiseRunner, SNRMode
from datetime import datetime
import sounddevice as sd

__author__ = 'jaime undurraga'


class Experiment(QObject):
    def __init__(self):
        super(Experiment, self).__init__()
        self.test_values = [0.0001, 0.0002]
        self.randomize = False
        self._value_index = 0
        self.reference_rms = 0.005
        self.reference_spl = 62.0
        self.runner = None
        self._test_values = self.test_values
        self.participant_id = ''
        self.participant_dob: datetime = None
        self.sound_device = 44
        self.sound_device_channels =[1, 2]

    def new_run(self, test_value=0):
        # play with this value for calibration and set staring snr to -300
        #  sets the runner
        if isinstance(self.runner, DigitsInNoiseRunner):
            del self.runner
        self.runner = DigitsInNoiseRunner()
        self.runner.signal_finish.connect(self.run)
        self.runner.tracker.participant_id = self.participant_id
        self.runner.tracker.participant_dob = self.participant_dob
        self.runner.tracker.down = 2
        self.runner.tracker.up = 1
        self.runner.tracker.step_sizes = [-6, -3, -2]
        self.runner.tracker.back_step_sizes = [6, 3, 2]
        self.runner.tracker.reversals_per_step = [2, 2, 6]
        self.runner.tracker.start_values = [10]
        self.runner.tracker.tracked_parameter = 'snr'
        self.runner.gui.finish_message = '{:} / {:}'.format(self._value_index + 1, len(self._test_values))
        noise_parameters = {'reference_rms': self.reference_rms,
                            'duration': 4.0}
        signal_parameters = {'itd': test_value, 'snr_mode': SNRMode.fix_masker}
        self.runner.tracker.extra_parameters = dict(noise_parameters, **signal_parameters)
        self.runner.noise_parameters = noise_parameters
        self.runner.signal_parameters = signal_parameters
        self.runner.reference_spl = self.reference_spl
        self.runner.number_of_digits = 3
        self.runner.sound_device = self.sound_device
        self.runner.sound_device_channels = self.sound_device_channels

    def run(self):
        if self._value_index == 0:
            self._test_values = self.test_values
            if self.randomize:
                self._test_values = random.sample(self.test_values, len(self.test_values))
        if self._value_index == len(self._test_values):
            return
        self.new_run(test_value=self._test_values[self._value_index])
        self._value_index += 1


if __name__ == '__main__':
    app = QtWidgets.QApplication(sys.argv)
    app.setStyle(QtWidgets.QStyleFactory.create('plastique'))
    app.setStyleSheet('QWidget{border: 1px solid gray; background-color: black; color: white}')
    ex = Experiment()
    ex.participant_id = 'JL'
    ex.participant_dob = datetime.strptime('Jun/9/1993', '%b/%d/%Y')
    #ex.test_values = ['inverse', 0, -0.0001, -0.0002, -0.0004, -0.0008]
    ex.test_values = [0]
    # do not touch these two values below as are used as the reference to set you at a desired level
    reference_spl = 65
    reference_rms = 0.021

    # input the desired level that you want to use
    desired_spl = 65

    ex.reference_spl = desired_spl
    ex.reference_rms = reference_rms * 10 ** ((desired_spl - reference_spl)/20)
    ex.randomize = True
    # to see available devices uncomment and run next line.
    # sd.query_devices()
    ex.sound_device = 'ASIO Fireface USB'
    # If you are using a mac you need use the channel map format eg [-1, -1, 0, 1, -1, -1] for channels 3 and 4s below
    ex.sound_device_channels = [2, 3]
    ex.run()
    sys.exit(app.exec_())

